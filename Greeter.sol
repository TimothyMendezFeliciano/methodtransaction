pragma solidity ^0.4.25;

contract Mortal {

	address owner;
	
	function Mortal() public {owner = msg.sender; }

	function kill() public { if(msg.sender == owner) selfdestruct(owner);}
}

contract Greeter is Mortal {
	string greeting = "Greetings, Officer K.";

	function Greeter(string _greeting) public {
	greeting = _greeting;
	}

	function greet() constant returns (string) {
	return greeting;
	}
}