var Tx = require('ethereumjs-tx')
const Web3 = require('web3')
const web3 = new Web3('http://127.0.0.1:7545')

const account1 = '0x3cDe3F8dA7192D3c3daBAfb6Ea0369175a248d94'
const account2 = '0x4CD4324d1EFe4De32BA944c836ad9aD54DB218D5'

const privateKey1 = Buffer.from('da1757d2d8667e9689d2d6d6cdb4745b452162625931228d0af3089588f40f2b', 'hex')
const privateKey2 = Buffer.from('2456174dcd9490498b02751d6f8e6c61f7a966bf3a892f18fa199e8167565eb2', 'hex')

const contractAddress = '0xA136d002656bc098933698d88b33E9f45a5ACE91'
const contractABI = [{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"greet","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_greeting","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]


const greeter = new web3.eth.Contract(contractABI, contractAddress)

const data = greeter.methods.greet().encodeABI()

const string = greeter.methods.greet()

web3.eth.getTransactionCount(account1, (err, txCount) => {

	// Create transaction object
	const txObject ={

		nonce: web3.utils.toHex(txCount),
		gasLimit: web3.utils.toHex(100000),
		gasPrice:web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
		to: contractAddress,
		data: data
	}

	//Sign transaction
	const tx = new Tx(txObject)
	tx.sign(privateKey1)

	const serializedTransaction = tx.serialize()
	const raw = '0x' + serializedTransaction.toString('hex')

	// Broadcast transaction
	web3.eth.sendSignedTransaction(raw, (err, txHash) => {
		console.log('err:', err, 'txHash:', txHash)
	})
})

